﻿#define SERVER // SERVER, TEST_CLIENT, TEST
#define WINDOWS // MAC, WINDOWS

using System;
using System.Collections.Generic;

using Network;

// 비정상 종료시 dump 생성하는 다른 방법
// https://m.blog.naver.com/websearch/221618132200

#if TEST
using Server.Lobby;
#endif


#if SERVER
using Server.Lobby;
#endif

#if CLIENT
using System.Net;
using Server.Client;
#endif

namespace Server
{
    class Program
    {
#if SERVER
        //private static List<string> userList;
        public static CLobbyServer lobbyServer = new CLobbyServer();
        
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnHandledException;

            CEnv.Init();
            CLogger.Init();

            lobbyServer.Start();
        }

        private static void UnHandledException(object s, UnhandledExceptionEventArgs e)
        {
#if MAC
            Exception exception = (Exception)e.ExceptionObject;

            var stringBuilder = new System.Text.StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            CLogger.Error(stringBuilder.ToString());
#endif

#if WINDOWS
            // 맥에선 실행 안됨
            var st = new System.Diagnostics.StackTrace((Exception)e.ExceptionObject, true);
            var frames = st.GetFrames();
            var stringBuilder = new System.Text.StringBuilder();

            stringBuilder.AppendLine();
            stringBuilder.AppendLine("============[Call Stack Trace End]============");
            stringBuilder.AppendLine("Error : " + ((Exception)e.ExceptionObject).Message);
            stringBuilder.AppendLine();

            foreach (var frame in frames)
            {
                if (frame.GetFileLineNumber() < 1)
                    continue;

                stringBuilder.AppendLine("[File] : " + frame.GetFileName());
                stringBuilder.AppendLine("[Method] : " + frame.GetMethod().Name + "()");
                stringBuilder.AppendLine("[LineNumber] : " + frame.GetFileLineNumber());
                stringBuilder.AppendLine("  ===========> ");
            }
            stringBuilder.Remove(stringBuilder.Length-17, 15);
            stringBuilder.AppendLine("============[Call Stack Trace Start]============");
            CLogger.Error(stringBuilder.ToString());
#endif
        }

#endif

#if CLIENT
        static ICallback gameServer;

        static void Main(string[] args)
        {
            Lobby.CEnv.Init();

            CPacketBufferManager.Init(2000);

            CServer server = new CServer(true);
            CConnector connector = new CConnector(server);
            connector.ConnectCallback += OnConnectedServer;

            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(Lobby.CEnv.GetString("HOST_IP")), Lobby.CEnv.GetInt("HOST_PORT"));
            connector.Connect(endPoint);

            while (true)
            {
                Console.WriteLine("[input 'q' is exit]");
                string line = Console.ReadLine();
                if (line == "q")
                {
                    break;
                }

                CPacket packet = CPacket.Create(EProtocolID.REQ_TEST);

                ReqPerson person = new ReqPerson();
                person.id = 1234;
                person.name = line;

                packet.Pack<ReqPerson>(person);

                gameServer.Send(packet);
            }

            gameServer.Disconnect();

            Console.ReadKey();

        }

        static void OnConnectedServer(CClientToken token)
        {
            ICallback server = new Client.Client(token);
            token.OnConnected();
            gameServer = server;
            Console.WriteLine("Connected");
        }    
#endif
    }
}

