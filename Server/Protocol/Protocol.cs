﻿using ProtoBuf;

namespace Network
{
    public enum ProtocolVersion : uint { PROTOCOL_VERSION = 1 }

    // 정경섭 2022.12.04
    // REQ : 무조건 클라에서부터 시작
    // REQ 가 없을경우 서버에서 클라로 이동하는 패킷

    [ProtoContract]
    public enum EProtocolID
    {
        // 개발 테스트
        [ProtoEnum] DUMMY = -1,
        [ProtoEnum] REQ_TEST,
        [ProtoEnum] ACK_TEST,

        // 로그인
        [ProtoEnum] REQ_LOGIN = 100,
        [ProtoEnum] RES_LOGIN,

        // 매칭
        [ProtoEnum] REQ_MATCHING = 200,
        [ProtoEnum] REQ_MATCHING_CANCEL,
        [ProtoEnum] RES_MATCHING_CANCEL,
        [ProtoEnum] RES_MATCHING_COMPLETED,

        // 게임
        // 정경섭 2022.12.04
        [ProtoEnum] GAME_START = 300,   // 게임 시작 알림 : 서버 -> 클라
        [ProtoEnum] REQ_GAME_PLAYING,   // 게임 진행중에 일어나는 액션들을 처리함  
        [ProtoEnum] RES_GAME_PLAYING,   // 게임 진행중에 일어나는 액션들을 처리함  
        [ProtoEnum] ROLE_CHANGE,        // 공수 교대 : 서버 -> 클라

        // 시스템
        [ProtoEnum] SYS_CLOSE_REQ = 65000,
        [ProtoEnum] SYS_CLOSE_ACK,
        [ProtoEnum] SYS_START_HEARTBEAT,
        [ProtoEnum] SYS_UPDATE_HEARTBEAT,
        [ProtoEnum] SYS_ERROR_CODE,
    }
   
    // 정경섭 2022.12.04
    //클라도 같이 써야 하는 공통 값
    enum PlayerStatus // 플레이어 상태 값
    {
        ATTACKER,
        DEFFENDER,
    }
    enum GameStatus // 게임 진행 상태 값
    {
        READY_ATTACK,   // 공격 준비 자세
        ATTACK,         // 공격
        DODGE,          // 회비
        ATTACK_SUCCESS, // 공격 성공
        ATTACK_FAIL,    // 공격 실패
        TIMES_UP,       // 제한 시간 지나 공수 교대
    };

    [ProtoContract]
    class ReqPerson
    {
        [ProtoMember(1)]
        public int id { get; set; }
        [ProtoMember(2)]
        public string name { get; set; }
    }

    [ProtoContract]
    class ResPerson
    {
        [ProtoMember(1)]
        public string txt { get; set; }
        [ProtoMember(2)]
        public string name { get; set; }
    }

    [ProtoContract]
    class HeartBeat
    {
        [ProtoMember(1)]
        public uint interval { get; set; }
    }

    [ProtoContract]
    class ReqLogin
    {
        [ProtoMember(1)]
        public uint protoNum { get; set; } // 이거그냥 임시용임. 프로토콜 버전확인용?
        [ProtoMember(2)]
        public string userName;
        [ProtoMember(3)]
        public string sessionID;
    }

    [ProtoContract]
    class ResLogin
    {
        [ProtoMember(1)]
        public string sessionID;
    }

    [ProtoContract]
    class ReqInputAction
    {
        [ProtoMember(1)]
        public int action;
    }


    [ProtoContract]
    class ResMatchCancel
    {

    }

    [ProtoContract]
    class ResMatchCompleted
    {

    }

    // 정경섭 2022.12.04
    [ProtoContract]
    class GameStart
    {
        [ProtoMember(1)]
        public Player player { get; set; } // 내가 공격자 인지 아닌지
    }

    [ProtoContract]
    class ReqGamePlaying   // 클라가 공격 상태를 서버로 보내줌
    {
        [ProtoMember(1)]
        public GameStatus gameStatus { get; set; }
    }

    [ProtoContract]
    class ResGamePlaying    // 공격에 성공을 해서 턴을 유지 할때만 발생
    {
        [ProtoMember(1)]
        public GameStatus gameStatus { get; set; }
        // 목숨은 몇개인가?
    }

    [ProtoContract]
    class RoleChange        // 공격 실패, 시간제한 등 공수 변경시에 발생
    {
        [ProtoMember(1)]
        public Player player { get; set; }          // 내가 공격자 인지 아닌지
        [ProtoMember(2)]
        public GameStatus gameStatus { get; set; }  // 어떤 이유에서 변경이 됐는지
    }

    [ProtoContract]
    class SysErrorCode
    {
        [ProtoMember(1)]
        public ERROR_CODE errorCode;
    }
}

