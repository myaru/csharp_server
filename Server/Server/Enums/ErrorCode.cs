﻿using System;

namespace Network
{
    public enum ERROR_CODE
    {
        FAIL,                       // 실패 (Default)
        SUCCESS,                    // 성공 (Default)

        INVALID_REQUEST,            // 유효하지 않은 요청
        INVALID_PROTOCOL_VERSION,   // 프토로콜 버전이 맞지 않음

        NOT_FOUND_USER,             // 유저를 찾지 못함
        NOT_LOGIN_USER,             // 로그인 중인 유저가 아님
        INVALID_USER_ID,            // 유효하지 않은 유저 ID

        SERVER_DISCONNECTED,        // 서버와 연결이 끊김
        NOT_ENOUGH_SERVER,          // 서버 수용 인원 여유가 없음

        ALREADY_MATCHING_COMPLETED, // 이미 매칭이 완료됨
    }
}

