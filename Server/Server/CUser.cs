﻿using System;
using System.Diagnostics;
using Network;

using Server.Lobby.Enums;
using Server.Lobby.Interface;

namespace Server.Lobby
{
	public class CUser : ICallback
	{
        //public Int64    uID;
        public string   nickName;

        private CClientToken    token;
        private USER_STATE_TYPE userState;
        private IUserState      userMessage;

        public Guid sessionID;
        public Guid roomID;

		public CUser(CClientToken token)
        { 
            this.token = token;
            this.token.SetCallback(this);         

            userMessage = new CUserState(this);
            userState   = USER_STATE_TYPE.LOBBY;
		}

        public void ChangeState(USER_STATE_TYPE state)
        {
            this.userState = this.userState == USER_STATE_TYPE.LOBBY ? USER_STATE_TYPE.LOGIC : USER_STATE_TYPE.LOBBY;
        }

        //<---- Interface Function
        public void OnMessage(CPacket packet)
        {
            this.userMessage.OnMessage(packet);
        }

        public void OnRemoved()
        {
            Debug.WriteLine("Client Disconnected");
            CLobbyServer.RemoveUser(this);
        }

        public void Disconnect()
        {
            this.token.Kick();
        }
        //---->

        public void Send(CPacket packet)
        {
            packet.SetHeader();

            byte[] tmp = new byte[packet.offset];
            Array.Copy(packet.buffer, tmp, packet.offset);

            this.token.Send(new ArraySegment<byte>(tmp, 0, packet.offset));
        }

        public void Send(EProtocolID protocol)
        {
            Send(CPacket.Create(protocol));
        }

        public void Send<T>(EProtocolID protocol, T protoStruct)
        {         
            var packet = CPacket.Create(protocol);
            packet.Pack(protoStruct);

            Send(packet);
        }
    }
}

