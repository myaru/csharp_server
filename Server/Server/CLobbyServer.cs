﻿using System;
using Network;

namespace Server.Lobby
{
	using global::Server.Network.DataBase;
	using Network;

	public class CLobbyServer
	{
		private string ip;
		private int port;

		public CLobbyServer()
		{
			this.ip = CEnv.GetString("HOST_IP");
			this.port = CEnv.GetInt("HOST_PORT");
        }

		public void Start()
        {
			CServer server = new CServer(true);
			server.SessionCreateCallback += OnSessionCreated;

			server.Init();
			server.Listen(this.ip, this.port, 100);
            
			if(CEnv.GetBool("DISABLE_HEARTBEAT")) // 하트비트 체크 비활성시 사용
                server.DisableHeartbeat(); 

			Console.WriteLine("-------------[Server Start]-------------");
			CLogger.Info("Server Start");

			ConsoleKeyInfo cki;
            while (true)
            {
				cki = Console.ReadKey(true);

				switch (cki.Key)
				{
					// 팡션키. 필요한 기능 있으면 여기에 추가하셈
					case ConsoleKey.F1: // 레디스 사용법
                        CRedis.instance.SampleRedisUseCase();
                        break;
					case ConsoleKey.F2: // 데이터 베이스 사용법
										//var t1 = CDataBase.instance.fsql.DbFirst.GetDatabases();

						// 조금더 자세한 설명은
						// https://www.cnblogs.com/FreeSql/p/11531381.html
						// 번역 필요함
						//var ret = CDataBase.instance.fsql.Select<Player>().ToList<Player>();
						//var ret = CDataBase.instance.Select2List<Player>();
						var ret = CDataBase.instance.Select<Player>().ToList();
						foreach(Player p in ret)
						{
                            Console.WriteLine($"{p.Id}, {p.Nickname}, {p.DBIndex}, {p.Rating}");
                        }						
						break;
					case ConsoleKey.F3:
						//CDataBase.instance.test();
						Player player = new Player { Nickname = "nyatest", DBIndex = 2, Rating = 999 };
						//CDataBase.instance.Insert<Player>(player);
						break;
					case ConsoleKey.F4:
						break;
					case ConsoleKey.F5:
						break;
					case ConsoleKey.F6:
						break;
					case ConsoleKey.F7:
						break;
					case ConsoleKey.F8:
						break;
					case ConsoleKey.F9:
						break;
					case ConsoleKey.F10:
						break;
					case ConsoleKey.F11:
						break;
					case ConsoleKey.F12:
						break;				
					default:
						break;
				}
			}
        }

		private void OnSessionCreated(CClientToken token)
        {					
			CUserManager.instacne.InsertUser(new CUser(token));
        }

		public static void RemoveUser(CUser user)
        {
			CUserManager.instacne.RemoveUser(user);
        }
	}
}

