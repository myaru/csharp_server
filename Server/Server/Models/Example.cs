﻿using System;
using FreeSql.DataAnnotations;

// 참고
// https://www.cnblogs.com/FreeSql/p/11531302.html
// 중국어임. 번역기 필수

namespace DontCall
{
    public class Example1
    {
        [Column(IsIdentity = true, IsPrimary = true)]
        public Int64 Id { get; set; }
        public string Nickname { get; set; }
        public int Rating { get; set; }
    }

    public class Example2
    {
        // Null 허용 안함1
        [Column(IsNullable = false)]
        public Int64 Id { get; set; }

        // Null 허용 안함2 (옵션을 안붙혀도 됨)
        public Int64 Ids { get; set; }

        // Null 허용1
        [Column(IsNullable = true)]
        public Int64 idss { get; set; }

        // Null 허용2
        public Int64? idsss { get; set; }

        // 컬럼에 기본값이 있는 경우(디폴트 값)
        [Column(IsIgnore = true)]
        public string Myinfo { get; set; }

        // DateTime 사용시
        [Column(ServerTime = DateTimeKind.Utc, CanUpdate = false)]
        public DateTime CreateTime { get; set; }

    }
}
