﻿using System;
using FreeSql.DataAnnotations;

public class Player
{
    [Column(IsIdentity = true, IsPrimary = true)]
    public Int64 Id { get; set; }
    public int DBIndex { get; set; }
    public string Nickname { get; set; }
    public int Rating { get; set; }
}
