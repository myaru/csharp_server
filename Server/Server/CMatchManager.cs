﻿using Network;
using Server.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Lobby
{
    public class CMatchInfo
    {
        public Guid         hostToken;
        public int          requiredCount;
        public List<Guid>   tokens;

        public CMatchInfo()
        {
            hostToken       = Guid.Empty;
            requiredCount   = 0;
            tokens          = new List<Guid>();
        }

        public CMatchInfo(Guid hostToken, int requiredCount)
        {
            this.hostToken      = hostToken;
            this.requiredCount  = requiredCount;
            this.tokens         = new List<Guid>();

            tokens.Add(hostToken);
        }        
    }

    public class CMatchManager
    {
        private static readonly Lazy<CMatchManager> _instance = new Lazy<CMatchManager>(() => new CMatchManager());
        public static CMatchManager instance { get { return _instance.Value; } }

        private Queue<CMatchInfo> _infos = null;

        public CMatchManager()
        {
            _infos = new Queue<CMatchInfo>();
        }

        public void register(Guid token, int requiredCount = 2)
        {
            lock (_infos)
            {
                if (empty())
                    createMatch(token, requiredCount);
                else
                    joinMatch(token);
            }
        }

        public bool cancelMatch(Guid token)
        {
            lock (_infos)
            {
                if (empty())
                    return false;

                var front = _infos.Peek();
                if (false == front.tokens.Contains(token))
                    return false;

                front.tokens.Remove(token);
                return true;
            }
        }

        private bool empty() 
        { 
            return 0 == _infos.Count; 
        }

        private void createMatch(Guid token, int requiredCount)
        {
            _infos.Enqueue(new CMatchInfo(token, requiredCount));
        }

        private void joinMatch(Guid token)
        {
            var front = _infos.Peek();
            front.tokens.Add(token);

            // 필요 인원을 충족하지 못했으면 Wait
            if (front.tokens.Count != front.requiredCount)
                return;

            List<CUser>     users       = new List<CUser>();
            Queue<Guid>     removeList  = new Queue<Guid>();

            foreach (var item in front.tokens)
            {
                var user = CUserManager.instacne.FindUser(item);
                if (null == user)                
                    removeList.Enqueue(item);                    
                else
                    users.Add(user);
            }

            // 찾을 수 없는 유저가 있으면 해당 유저 ID 제거
            if (0 != removeList.Count)
            {
                foreach (var item in removeList)
                    front.tokens.Remove(item);

                if (0 == front.tokens.Count)
                    _infos.Dequeue();
                return;
            }

            // 모든 유저가 로그인 중이면 매칭 완료임으로 패킷 송신 및 게임 룸 준비
            var newRoomID = CRoomManager.instance.genRoomID();
            foreach (var user in users)
            {
                user.roomID = newRoomID;
                CRoomManager.instance.insertUser(newRoomID, user);
                user.Send(EProtocolID.RES_MATCHING_COMPLETED);
            }
            _infos.Dequeue();
        }       
    }
}
