﻿using Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Lobby
{

    public class CUserManager
    {
        private static readonly Lazy<CUserManager> _instance = new Lazy<CUserManager>(() => new CUserManager());
        public static CUserManager instacne { get { return _instance.Value; } }

        private List<CUser> _users;

        private CUserManager()
        {
            _users = new List<CUser>();
        }

        public CUser FindUser(Guid token)
        {
            lock (_users)
                return _users.Find(user => user.sessionID == token);
        }

        public bool InsertUser(CUser user)
        {
            lock (_users)
            {
                _users.Add(user);
                return true;
            }
        }

        public void RemoveUser(CUser user)
        {
            lock (_users)
                _users.Remove(user);
        }

        public void Broadcast(CPacket packet)
        {
            lock (_users)
            {
                foreach (var user in _users)
                    user.Send(packet);
            }
        }
    }
}
