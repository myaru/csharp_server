﻿using System;
using System.Security.AccessControl;
using Network;

namespace Server.Lobby.Interface
{
	public class CUserState : IUserState
	{
        CUser user;

		public CUserState(CUser user)
		{          
            this.user = user;
		}

        public void OnMessage(CPacket packet)
        {
            Console.WriteLine("[Recv] " + packet.protocolID);

            switch (packet.protocolID)
            {
                case EProtocolID.REQ_TEST:              OnReceiveTest(packet);          break;
               
                case EProtocolID.REQ_LOGIN:             OnReceiveLogin(packet);         break;
                case EProtocolID.REQ_MATCHING:          OnReceiveMatching(packet);      break;
                case EProtocolID.REQ_MATCHING_CANCEL:   OnReceiveMatchCancel(packet);   break;
            }
        }

        private void OnReceiveTest(CPacket packet)
        {
            var response = CPacket.Create(EProtocolID.ACK_TEST);

            ReqPerson reqPerson = packet.UnPack<ReqPerson>();
            Console.WriteLine("ID : " + reqPerson.id + ", Name : " + reqPerson.name);

            ResPerson person = new ResPerson();
            person.txt = "Good JoBㄱㄴㄷㄹ1234!@#$";
            person.name = reqPerson.name;
            response.Pack<ResPerson>(person);

            this.user.Send(response);
        }

        private void OnReceiveLogin(CPacket packet)
        {
            ReqLogin reqLogin = packet.UnPack<ReqLogin>();
           
            if (reqLogin.protoNum != (uint)ProtocolVersion.PROTOCOL_VERSION)
            {
                SendErroCode(ERROR_CODE.INVALID_PROTOCOL_VERSION);
                return;
            }

            user.nickName = reqLogin.userName;
            if (reqLogin.sessionID == null)
                user.sessionID = Guid.NewGuid();
            else
                user.sessionID = Guid.Parse(reqLogin.sessionID);

            var res = new ResLogin();
            res.sessionID = user.sessionID.ToString();

            user.Send(EProtocolID.RES_LOGIN, res);
        }

        private void OnReceiveMatching(CPacket packet)
        {
            CMatchManager.instance.register(user.sessionID);
        }

        private void OnReceiveMatchCancel(CPacket packet)
        {
            if (false == CMatchManager.instance.cancelMatch(user.sessionID))
            {
                SendErroCode(ERROR_CODE.ALREADY_MATCHING_COMPLETED);
                return;
            }
            user.Send(EProtocolID.RES_MATCHING_CANCEL);
        }

        public void SendErroCode(ERROR_CODE errorCode)
        {
            var errorInfo = new SysErrorCode();
            errorInfo.errorCode = errorCode;

            user.Send(EProtocolID.SYS_ERROR_CODE, errorInfo);
        }
    }
}

