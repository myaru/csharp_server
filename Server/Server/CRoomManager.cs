﻿using Network;
using Server.Lobby;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Server
{
    public class CRoom
    {
        public Dictionary<Guid, CUser> users = new Dictionary<Guid, CUser>();
        public Guid index;

        public void broadcast<T>(EProtocolID protocol, T data, Guid senderID) where T : class
        { 
            foreach (var item in users)
            {
                if (null == item.Value)
                    continue;
                if (item.Key == senderID)
                    continue;

                item.Value.Send(protocol, data);
            }
        }
    }

    public class CRoomManager
    {
        private static readonly Lazy<CRoomManager> _instance = new Lazy<CRoomManager>(() => new CRoomManager());
        public static CRoomManager instance { get { return _instance.Value; } }

        private Dictionary<Guid, CRoom> _rooms = new Dictionary<Guid, CRoom>();
        
        public Guid genRoomID() { return Guid.NewGuid(); }

        public void insertUser(Guid guid, CUser user)
        {
            lock (_rooms)
                _rooms[guid].users.Add(user.sessionID, user);
        }

        public void removeUser(Guid guid, CUser user)
        {
            lock (_rooms)
                _rooms[guid].users.Remove(user.sessionID);
        }

        public void broadcast<T>(Guid guid, EProtocolID protocol, T data) where T : class
        {
            lock (_rooms)
                _rooms[guid].broadcast(protocol, data, Guid.Empty);
        }

        public void othersSend<T>(Guid guid, EProtocolID protocol, T data, Guid senderID) where T : class
        {
            lock (_rooms)
                _rooms[guid].broadcast(protocol, data, senderID);
        }

        public void destroyRoom(Guid guid)
        {
            lock (_rooms)
                _rooms.Remove(guid);
        }
    }
}
