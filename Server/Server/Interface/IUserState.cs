﻿using Network;

namespace Server.Lobby.Interface
{
	interface IUserState
    {
        void OnMessage(CPacket packet);
    }
}

