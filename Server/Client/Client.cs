﻿using System;

using Network;

namespace Server.Client
{
    public class Client : ICallback
    {
        public CClientToken token { get; private set; }
        private int recvCount = 0;

        public Client(CClientToken token)
        {
            this.token = token;
            this.token.SetCallback(this);
        }

        void ICallback.Disconnect()
        {
            this.token.Disconnect();
        }

        void ICallback.OnMessage(CPacket packet)
        {
            System.Threading.Interlocked.Increment(ref this.recvCount);

            Console.WriteLine("[Recv] " + packet.protocolID);

            switch (packet.protocolID)
            {
                case EProtocolID.ACK_TEST:
                    {
                        ResPerson person = packet.UnPack<ResPerson>();
                        Console.WriteLine(string.Format("[ResPonse] txt : {0}, name : {1}", person.txt, person.name));
                    }
                    break;
            }

        }

        void ICallback.OnRemoved()
        {
            Console.WriteLine("Server Removed");
            Console.WriteLine("Recv Cnt = " + this.recvCount);
        }

        void ICallback.Send(CPacket packet)
        {
            packet.SetHeader();
            this.token.Send(new ArraySegment<byte>(packet.buffer, 0, packet.offset));
        }
    }
}

