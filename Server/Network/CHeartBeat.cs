﻿using System;
using System.Threading;

using ProtoBuf;

namespace Network
{
	public class CHeartBeat
	{
		CClientToken session;
		Timer heartBeatTimer;
		uint interval;

		float elapsedTime;

		public CHeartBeat(CClientToken session, uint interval)
		{
			this.session = session;
			this.interval = interval;
			this.heartBeatTimer = new Timer(this.OnTimer, null, Timeout.Infinite, this.interval * 1000);
		}

		private void OnTimer(object state)
        {
			Send();
        }

		private void Send()
        {
			CPacket packet = CPacket.Create(EProtocolID.SYS_UPDATE_HEARTBEAT);
			this.session.Send(packet);
        }

		public void Update(float time)
        {
			this.elapsedTime += time;
			if (this.elapsedTime < this.interval)
				return;

			this.elapsedTime = 0.0f;
			Send();
        }

        public void Stop()
        {
			this.elapsedTime = 0;
			this.heartBeatTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

		public void Start()
        {
			this.elapsedTime = 0;
			this.heartBeatTimer.Change(0, this.interval * 1000);
        }
	}
}

