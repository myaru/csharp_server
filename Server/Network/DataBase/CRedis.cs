﻿using System;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Network
{    
         
    public class CRedis
    {
        private static readonly Lazy<CRedis> _instance = new Lazy<CRedis>(() => new CRedis());

        public static CRedis instance { get { return _instance.Value; } }

        private ConnectionMultiplexer redis;

        public delegate void Pub();
        public delegate void Sub();

        public IDatabase db;
        public bool isRun;

        public CRedis()
        {
            try
            {
                string addr = $"{CEnv.GetString("REDIS_HOST")}:{CEnv.GetString("REDIS_PORT")},allowAdmin=true";
                redis = ConnectionMultiplexer.Connect(addr);
                isRun = true;

                if (redis.IsConnected)
                    db = redis.GetDatabase();
                else
                    throw new Exception("Init Fail");
            }
            catch (Exception e)
            {
                CLogger.Error("[Redis] Redis error", e);
                isRun = false;
            }
        }

        private class Test
        {
            public string Data { get; set; }

            public Test(string data)
            {
                Data = data;
            }

            public void Print()
            {
                Console.WriteLine(Data);
            }
        }

        public void SampleRedisUseCase()
        {
            Console.Clear();
            // Set
            Console.WriteLine("How to Set---------------");
            db.StringSet("a1", 12345);
            db.StringSet("a2", "abcd123");

            // Get
            Console.WriteLine("How to Get---------------");
            db.StringGet("a1");
            db.StringGet("a2");

            // Class Serialize
            Console.WriteLine("How to Serialize class---------------");
            Test test = new Test("Test");
            var data = JsonConvert.SerializeObject(test);
            db.StringSet("Test", data);

            Console.WriteLine(db.StringGet("Test"));

            // Deserialize
            Console.WriteLine("How to Deserialize class---------------");
            var getData = db.StringGet("Test");
            Test deSerializeTest = JsonConvert.DeserializeObject<Test>(getData);
            deSerializeTest.Print();

            // Expire
            Console.WriteLine("How to Expire---------------");
            db.StringSet("expirre", 12, TimeSpan.FromSeconds(5));
            Console.WriteLine("5초 뒤에 redis-cli에서 get expirre 쳐보셈");
        }
    }
}

