﻿using FreeSql;
using MySqlConnector;
using Network;
using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Server.Network.DataBase
{
    internal class CDataBase
    {
        public enum CODE
        {
            DUPLICATE = -100,
        }

        public static readonly Lazy<CDataBase> _instance = new Lazy<CDataBase>(() => new CDataBase());

        public static CDataBase instance { get { return _instance.Value; } }

        public IFreeSql fsql;
        public bool isRun;
                            
        public CDataBase()
        {
            try
            {
                string adr = $"Data Source={CEnv.GetString("GAME_DB_HOST")};Port={CEnv.GetString("GAME_DB_PORT")};User ID={CEnv.GetString("GAME_DB_USERNAME")};Password={CEnv.GetString("GAME_DB_PASSWORD")};Initial Catalog={CEnv.GetString("GAME_DB")};Charset=utf8mb4; SslMode=none;Max pool size={CEnv.GetInt("GAME_DB_POOL_SIZE")}";
                
                fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, adr)
                .UseAutoSyncStructure(true)
                .Build();
                isRun = true;
            }
            catch (Exception e)
            {
                CLogger.Error("[MySql] Mysql error", e);
                isRun = false;
            }            
        }

        public ISelect<T> Select<T>() where T : class, new()
        {
            return fsql.Select<T>();
        }

        public List<T> Select2List<T>() where T : class, new()
        {
            List<T> ret = fsql.Select<T>().ToList<T>();            
            return ret;
        }

        public IInsert<T> Insert<T>() where T : class, new()
        {
            return fsql.Insert<T>();
        }

        /// <summary>
        /// Insert <typeparamref name="Models"/>
        /// => 중복값 혹은 기타 에러를 뱉을 수 있음
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>추가된 데이터 수, 혹은 에러코드</returns>
        public int InsertS<T>(T data) where T : class, new()
        {
            try
            {
                return fsql.Insert(data).ExecuteAffrows();
            }
            catch (Exception)
            {
                // 일단 중복값만 있다고 가정하겠음
                return (int)CODE.DUPLICATE;
            }

            // 이렇게 써도 에러나는건 같음
            // 그냥 안전하게 이거 써야하나
            //fsql.Transaction(() =>
            //{
            //    fsql.Insert(data).ExecuteAffrows();
            //});
        }

        public IUpdate<T> Update<T>() where T : class
        {
            return fsql.Update<T>();
        }

        public void UpdateS<T>(T data) where T : class
        {
            var ret = fsql.Update<T>().SetSource(data).ExecuteAffrows();
        }

        public void UpdateWithTranscation<T>(T data) where T : class
        {
            fsql.Transaction(() =>
            {

            });
        }
    }
}

