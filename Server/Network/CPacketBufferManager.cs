﻿using System;
using System.Collections.Generic;

namespace Network
{
	public class CPacketBufferManager
	{
		private static object lock_Buffer = new object();
        private static Stack<CPacket> pool;
        private static int poolSize;

		public static void Init(int size)
        {
			pool = new Stack<CPacket>();
			poolSize = size;
			Alloc();
        }

		private static void Alloc()
        {
			for(int i = 0; i < poolSize; i++)
            {
				pool.Push(new CPacket());
            }
        }

		public static CPacket Pop()
        {
			lock(lock_Buffer)
            {
				if(pool.Count <= 0)
                {
                    Console.WriteLine("ReAllocated.");
                    Alloc();
                }

                return pool.Pop();
            }
        }

        public static void Push(CPacket packet)
        {
            lock(lock_Buffer)
            {
                pool.Push(packet);
            }
        }

	}
}

