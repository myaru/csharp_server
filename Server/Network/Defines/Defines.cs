﻿using System;

namespace Network
{
	public class Defines
	{
		public static readonly short HEADER_SIZE = 4;

		public static readonly int PACKET_SIZE = 2048;

		public static readonly int MAX_CONNECTION = 10000;

		public static readonly uint HEART_BEAT_TIME = 5; // 초단위
	}
}

