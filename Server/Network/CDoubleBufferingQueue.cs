﻿using System.Collections.Generic;

namespace Network
{
    /// <summary>
    /// 두개의 큐를 교체해가며 활용한다.
    /// IO스레드에서 입력큐에 막 쌓아놓고,
    /// 로직스레드에서 큐를 뒤바꾼뒤(swap) 쌓아놓은 패킷을 가져가 처리한다.
    /// 참고 : http://roadster.egloos.com/m/4199854
    /// </summary>
    class CDoubleBufferingQueue : ILogicQueue
    {
        // 실제 데이터가 들어갈 큐.
        Queue<CPacket> queue1;
        Queue<CPacket> queue2;

        // 각각의 큐에 대한 참조.
        Queue<CPacket> refInput;
        Queue<CPacket> refOutput;

        object writeLock;


        public CDoubleBufferingQueue()
        {
            // 초기 세팅은 큐와 참조가 1:1로 매칭되게 설정한다.
            // ref_input - queue1
            // ref)output - queue2
            this.queue1 = new Queue<CPacket>();
            this.queue2 = new Queue<CPacket>();
            this.refInput = this.queue1;
            this.refOutput = this.queue2;

            this.writeLock = new object();
        }


        /// <summary>
        /// IO스레드에서 전달한 패킷을 보관한다.
        /// </summary>
        /// <param name="msg"></param>
        void ILogicQueue.Enqueue(CPacket msg)
        {
            lock (this.writeLock)
            {
                this.refInput.Enqueue(msg);
            }
        }


        Queue<CPacket> ILogicQueue.GetAll()
        {
            Swap();
            return this.refOutput;
        }


        /// <summary>
        /// 입력큐와 출력큐를 뒤바꾼다.
        /// </summary>
        private void Swap()
        {
            lock (this.writeLock)
            {
                Queue<CPacket> temp = this.refInput;
                this.refInput = this.refOutput;
                this.refOutput = temp;
            }
        }
    }
}

