﻿using System;

namespace Network
{
	public interface IMessageDispatcher
	{
		void OnMessage(CClientToken userSession, ArraySegment<byte> buffer);
	}
}

