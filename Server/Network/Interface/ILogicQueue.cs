﻿using System.Collections.Generic;

namespace Network
{
	public interface ILogicQueue
	{
		void Enqueue(CPacket msg);
		Queue<CPacket> GetAll();
	}
}

