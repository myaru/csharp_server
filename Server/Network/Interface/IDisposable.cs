﻿namespace Network
{
    public interface IDisposable
    {
        void Dispose();
    }
}