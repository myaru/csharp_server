﻿using System;

namespace Network
{
	/// <summary>
    /// 서버와 클라이언트에서 공통으로 사용하는 콜백
    ///
    /// 서버 :
    /// 하나의 클라이언트 객체를 뜻함
    ///
    /// 클라 :
    /// 접속한 서버 객체를 뜻함
    /// </summary>
	public interface ICallback
	{
		// CServer.Init에서 logicthread 사용시
		// true일때
		// IO스레드(스레드 풀)에서 호출
		//
		// flase일때
		// 로직 스레드에서 호출, 로직 스레드는 싱글 스레드로 돌아감
		void OnMessage(CPacket packet);

		// 소켓 연결이 끊겼을때 호출
		void OnRemoved();

		void Send(CPacket packet);

		void Disconnect();
	}
}

