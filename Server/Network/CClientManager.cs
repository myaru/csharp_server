﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Network
{
	public class CClientManager
	{
		object lock_client;
		List<CClientToken> clientTokens;

		// 하트비트 타이머₩
		Timer heartbeat_timer;
		long heartbeat_duration;

		public CClientManager()
		{
			this.lock_client = new object();
			this.clientTokens = new List<CClientToken>();
		}

		/// <summary>
        /// 하트비트 설정
        /// </summary>
        /// <param name="checkInterval">확인 간격(초)</param>
        /// <param name="allowDuration"></param>
		public void StartHeartbeatCheck(uint checkInterval, uint allowDuration)
        {
			this.heartbeat_duration = allowDuration * 10000000;
			this.heartbeat_timer = new Timer(CheckHeartbeat, null, 1000 * checkInterval, 1000 * checkInterval);
        }

		public void StopHeartbeatCheck()
        {
			this.heartbeat_timer.Dispose();
        }

		public void CheckHeartbeat(object state)
        {
			long allowTime = DateTime.Now.Ticks - this.heartbeat_duration;

			lock (this.lock_client)
            {
				for(int i = 0; i < this.clientTokens.Count; i++)
                {
					long heartbeatTime = this.clientTokens[i].lastHeartBeatTime;
					if (heartbeatTime >= allowTime)
						continue;
					
					this.clientTokens[i].Disconnect();
                }
            }
        }

		public void Add(CClientToken token)
        {
			lock (this.lock_client)
				this.clientTokens.Add(token);
        }

		public void Remove(CClientToken token)
        {
			lock (this.lock_client)
				this.clientTokens.Remove(token);
        }

		public bool isExits(CClientToken token)
        {
			lock (this.lock_client)
				return this.clientTokens.Exists(obj => obj == token);
        }

		public int GetTotalCount()
        {
			return this.clientTokens.Count;
        }
	}
}

