﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Network
{
	public class CConnector
	{
		public delegate void ConnectHandler(CClientToken token);
		public ConnectHandler ConnectCallback { get; set; }

		Socket client;
		CServer server;

		public CConnector(CServer server)
		{
			this.server = server;
			this.ConnectCallback = null;
		}

		public void Connect(IPEndPoint remoteEndPoint)
        {
			this.client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			this.client.NoDelay = true;

			// 비동기 접속
			SocketAsyncEventArgs eventArgs = new SocketAsyncEventArgs();
			eventArgs.Completed += OnConnectComplete;
			eventArgs.RemoteEndPoint = remoteEndPoint;

			bool pending = this.client.ConnectAsync(eventArgs);
			if (!pending)
				OnConnectComplete(null, eventArgs);
        }

		private void OnConnectComplete(object sender, SocketAsyncEventArgs e)
        {
			if(e.SocketError == SocketError.Success)
            {
				// token은 현재 접속한 서버
				CClientToken token = new CClientToken(this.server.logicThread);

				// 접속 완료 콜백을 전달
                if (this.ConnectCallback != null)
                    this.ConnectCallback(token);

				// 패킷 수신 준비
                this.server.OnConnectCompleted(this.client, token);
            }
			else
            {
				Console.WriteLine(string.Format("Failed to connect. {0}", e.SocketError));
            }
        }

    }
}

