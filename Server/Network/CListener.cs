﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Network
{
	public class CListener
	{
		// 비동기 Accept을 위한 EventArgs
		private SocketAsyncEventArgs acceptArgs;

		private Socket listenSocket;

		private AutoResetEvent acceptFlowControlEvent;

		public delegate void NewClientHandler(Socket clientSocket, object token);
		public NewClientHandler callbackOnNewClient;

		public CListener()
		{
			this.callbackOnNewClient = null;
		}

		public void Start(string host, int port, int backlog)
        {
			this.listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			IPAddress address;
			if (host == "0.0.0.0")
				address = IPAddress.Any;
			else
				address = IPAddress.Parse(host);

			IPEndPoint endPoint = new IPEndPoint(address, port);

            try
            {
				listenSocket.Bind(endPoint);
				listenSocket.Listen(backlog);

				this.acceptArgs = new SocketAsyncEventArgs();
				this.acceptArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnAcceptCompleted);

				Thread listenThread = new Thread(DoListen);
				listenThread.Start();
            }
            catch (Exception ex)
            { }
		}

		private void DoListen()
        {
			this.acceptFlowControlEvent = new AutoResetEvent(false);

            while (true)
            {
				this.acceptArgs.AcceptSocket = null;

				bool pending = true;
                try
                {
					pending = listenSocket.AcceptAsync(this.acceptArgs);
                }
                catch (Exception ex)
                {
					continue;
                }

				if (!pending)
					OnAcceptCompleted(null, this.acceptArgs);

				this.acceptFlowControlEvent.WaitOne();

            }
        }

		private void OnAcceptCompleted(object sender, SocketAsyncEventArgs e)
        {
			if(e.SocketError == SocketError.Success)
            {
				Socket clientSocket = e.AcceptSocket;
				clientSocket.NoDelay = true;

				// 리스너 클래스는 aceept까지만 수행하고 클라이언트의 접속 이후 처리는 다른 클래스에서 처리함			
				if (this.callbackOnNewClient != null)
					this.callbackOnNewClient(clientSocket, e.UserToken);

				// 다음 연결받을 수 있게 함
				this.acceptFlowControlEvent.Set();
				return;
            }
            else
            {
                Console.WriteLine("Failed to accept client. " + e.SocketError);
            }

			this.acceptFlowControlEvent.Set();
        }
		
	}
}

