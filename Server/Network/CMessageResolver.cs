﻿using System;

namespace Network
{
    public delegate void CompletedMessageCallback(ArraySegment<byte> buffer);

    public class CMessageResolver
	{
		// 패킷 사이즈
		private int msgSize;

		// 진행중인 버퍼
		private byte[] buffer = new byte[Defines.PACKET_SIZE];

		// 현재 진행중인 버퍼의 인덱스를 가르킴, 패킷하나 완성 후 0으로 초기화 해야함
		private int curPos;

		// 읽어와야 할 목표 위치
		private int posToRead;

		// 남은 사이즈
		private int remainSize;

		public CMessageResolver()
		{
			msgSize = 0;
			curPos = 0;
			posToRead = 0;
			remainSize = 0;
		}

		/// <summary>
        /// 소켓 버퍼에서 데이터를 수신할때마다 호출됨
        /// 여기서 헤더까지 읽고 c++로 따지면 포인터를 프로토콜이 있는 지점으로 옮겨짐.
        /// 콜백으로 호출한 함수에선 프로토콜 ID부터 파싱이 가능함
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="transfferd"></param>
        /// <param name="callback"></param>
		public void OnRecv(byte[] buffer, int offset, int transfferd, CompletedMessageCallback callback)
        {
			// 이번(호출된 시점) Recv로 읽어오게 될 바이트 수
			this.remainSize = transfferd;

			// 원본 버퍼의 포지션 값 => 패킷이 여러개 뭉쳐 올 경우 원본 버퍼의 포지션은 계속 앞으로 가게끔 처리하는 변수임
			int srcPos = offset;

			// 남은 데이터가 있으면 계속 반복
            while (this.remainSize > 0)
            {
				bool completed = false;

				// 헤더만큼 못읽은 경우 헤더를 먼저 읽음
				if (this.curPos < Defines.HEADER_SIZE)
                {
					// 헤더 사이즈 만큼 읽게끔 위치 지정
					this.posToRead = Defines.HEADER_SIZE;

					completed = ReadUntil(buffer, ref srcPos);
					if (!completed)
						return; // 다 못읽었으니 다음 recv를 기다림

					// 헤더를 io버퍼에서 읽어왔으니 메시지의 총 크기를 구함
					this.msgSize = GetMessageSize();
					if(this.msgSize <= 0)
                    {
						ClearBuffer();
						return;
                    }

					// 다음 목표 설정 (그러면 프로토콜ID부터 메시지만 남음)
					this.posToRead = this.msgSize;
					
					// 헤더를 다 읽었는데 더이상 가져올 데이터가 없으면 다음 recv를 기다림 (예 : 패킷이 조각나서 헤더만 왔을 경우 기다리는 용)
					if (this.remainSize <= 0)
						return;
                }

				completed = ReadUntil(buffer, ref srcPos);
				if (completed)
                {
					// 패킷을 만들고 다음단계로 넘김
					byte[] clone = new byte[this.posToRead];
					Array.Copy(this.buffer, clone, this.posToRead);
					ClearBuffer();
					callback(new ArraySegment<byte>(clone, 0, this.posToRead));
                }
            }
        }

		/// <summary>
		/// 목표지점으로 설정된 위치까지의 바이트를 원본 버퍼로부터 복사함. 데이터가 부족할 경우 현재 남은 바이트까지 복사함
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="srcPos"></param>
		/// <returns>다 읽었으면 true, 데이터가 부족하면 false 리턴</returns>
		private bool ReadUntil(byte[] buffer, ref int srcPos)
		{
			// 읽어와야 할 바이트
			// 데이터가 나뉘어서 올 경우 이전에 읽어놓은 값을 뺀뒤 부족한 만큼 읽어올 수 있게 계산함
			int copySize = this.posToRead - this.curPos;

			// 남은 데이터가 더 작다면 가능한 만큼만 복사함
			if (this.remainSize < copySize)
				copySize = this.remainSize;

			// 버퍼에 복사
			Array.Copy(buffer, srcPos, this.buffer, this.curPos, copySize);

			// 원본 버퍼 포지션으로 이동
			srcPos += copySize;

			// 타겟 버퍼 포지션도 이동
			this.curPos += copySize;

			// 남은 바이트 수
			this.remainSize -= copySize;

			// 목표지점에 도달 못했으면 return false
			if (this.curPos < this.posToRead)
				return false;

			return true;
		}

		private int GetMessageSize()
        {
			Int32 size = BitConverter.ToInt32(this.buffer, 0);
			if(size < Defines.HEADER_SIZE)
				return 0;

			return size;
        }

		public void ClearBuffer()
        {
			Array.Clear(this.buffer, 0, this.buffer.Length);
			this.curPos = 0;
			this.msgSize = 0;
        }

	}
}

