﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Network
{
	public class CServer
	{
		private SocketAsyncEventArgsPool RecvEventArgsPool;
		private SocketAsyncEventArgsPool SendEventArgsPool;

		public delegate void SessionHandler(CClientToken token);
		public SessionHandler SessionCreateCallback { get; set; }

		public CLogicThread logicThread { get; private set; }
		public CClientManager clientManager { get; private set; }

		public CServer(bool useThread = false)
        {
			this.SessionCreateCallback = null;
			this.clientManager = new CClientManager();

			if (useThread)
            {
				this.logicThread = new CLogicThread(this);
				this.logicThread.Strat();
            }
        }

		public void Init()
        {
			int preAllocCount = 1;			
			
            BufferManager bufferManager = new BufferManager(Defines.MAX_CONNECTION * Defines.PACKET_SIZE * preAllocCount, Defines.PACKET_SIZE);
			this.RecvEventArgsPool = new SocketAsyncEventArgsPool(Defines.MAX_CONNECTION);
			this.SendEventArgsPool = new SocketAsyncEventArgsPool(Defines.MAX_CONNECTION);

			bufferManager.InitBuffer();

			SocketAsyncEventArgs arg;

			for(int i = 0; i < Defines.MAX_CONNECTION; i++)
            {
				// Recv Pool
                {
					arg = new SocketAsyncEventArgs();
					arg.Completed += new EventHandler<SocketAsyncEventArgs>(RecvCompleted);
					arg.UserToken = null;

					bufferManager.SetBuffer(arg);

					this.RecvEventArgsPool.Push(arg);
                }

				// Send Pool
                {
					arg = new SocketAsyncEventArgs();
					arg.Completed += new EventHandler<SocketAsyncEventArgs>(SendCompleted);
					arg.UserToken = null;

					arg.SetBuffer(null, 0, 0);

					this.SendEventArgsPool.Push(arg);
                }
            }
        }

		public void Listen(string host, int port, int backlog)
        {
			CListener clientListner = new CListener();
			clientListner.callbackOnNewClient = OnNewClient;
			clientListner.Start(host, port, backlog);

            //uint checkInterval = Defines.HEART_BEAT_TIME * 2;
            uint checkInterval = Defines.HEART_BEAT_TIME;
            this.clientManager.StartHeartbeatCheck(checkInterval, checkInterval);
        }

		public void DisableHeartbeat()
        {
			this.clientManager.StopHeartbeatCheck();
        }

		/// <summary>
        /// 클라가 서버에 접속 성공했을때 호출
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="token"></param>
		public void OnConnectCompleted(Socket socket, CClientToken token)
        {
			token.OnSessionClosed += this.OnSessionClosed;
			this.clientManager.Add(token);

			SocketAsyncEventArgs recvArgs = new SocketAsyncEventArgs();
			recvArgs.Completed += new EventHandler<SocketAsyncEventArgs>(RecvCompleted);
			recvArgs.UserToken = token;
			recvArgs.SetBuffer(new byte[Defines.PACKET_SIZE], 0, Defines.PACKET_SIZE);

			SocketAsyncEventArgs sendArgs = new SocketAsyncEventArgs();
			sendArgs.Completed += new EventHandler<SocketAsyncEventArgs>(SendCompleted);
			sendArgs.UserToken = token;
			sendArgs.SetBuffer(null, 0, 0);

			BegineRecv(socket, recvArgs, sendArgs);
        }

		/// <summary>
        /// 새로운 클라이언트가 접속했을 때 호출
        /// </summary>
        /// <param name="clientSocket"></param>
        /// <param name="token"></param>
		private void OnNewClient(Socket clientSocket, object token)
        {
			// 풀에서 하나씩 꺼내다 씀
			SocketAsyncEventArgs recvArgs = this.RecvEventArgsPool.Pop();
			SocketAsyncEventArgs sendArgs = this.SendEventArgsPool.Pop();

			CClientToken clientToken = new CClientToken(this.logicThread);
			clientToken.OnSessionClosed += this.OnSessionClosed;
			recvArgs.UserToken = clientToken;
			sendArgs.UserToken = clientToken;

            this.clientManager.Add(clientToken);

			clientToken.OnConnected();
			if (this.SessionCreateCallback != null)
				this.SessionCreateCallback(clientToken);

			BegineRecv(clientSocket, recvArgs, sendArgs);

            // 클라이언트로 5초마다 한번씩 하트빗 패킷 쏘라고 날려줌
            {
                CPacket packet = CPacket.Create(EProtocolID.SYS_START_HEARTBEAT);
                HeartBeat heartBeat = new HeartBeat { interval = Defines.HEART_BEAT_TIME };
                packet.Pack<HeartBeat>(heartBeat);
                clientToken.Send(packet);
            }            

			Console.WriteLine("[Connect] " + clientToken.ip + ":" + clientToken.port);
			Console.WriteLine("[Send] Start HeartBeat " + clientToken.ip + ":" + clientToken.port);
        }

		private void BegineRecv(Socket socket, SocketAsyncEventArgs recvArgs, SocketAsyncEventArgs sendArgs)
        {
			CClientToken token = recvArgs.UserToken as CClientToken;
			token.SetEventArgs(recvArgs, sendArgs);

			token.socket = socket;
			token.ip = ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
			token.port = ((IPEndPoint)socket.RemoteEndPoint).Port.ToString();

            bool pending = socket.ReceiveAsync(recvArgs);
			if (!pending)
				ProcessRecv(recvArgs);
        }

		// Recv, Send Completed는 여기서 참고함
		// https://docs.microsoft.com/ko-kr/dotnet/api/system.net.sockets.socketasynceventargs?view=net-5.0
		private void RecvCompleted(object sender, SocketAsyncEventArgs e)
        {
			if (e.LastOperation == SocketAsyncOperation.Receive)
            {
				ProcessRecv(e);
				return;
            }

			throw new ArgumentException("The last operation completed on the socket was not a receive.");
        }

		private void SendCompleted(object sender, SocketAsyncEventArgs e)
        {
            try
            {
				CClientToken session = e.UserToken as CClientToken;
				session.ProcessSend(e);
            }
            catch (Exception ex)
            {
				CLogger.Error(ex.Message);
			}
        }

		private void ProcessRecv(SocketAsyncEventArgs e)
        {
			CClientToken token = e.UserToken as CClientToken;

			if(e.BytesTransferred > 0 && e.SocketError == SocketError.Success)
            {
				token.OnRecv(e.Buffer, e.Offset, e.BytesTransferred);

				bool pending = token.socket.ReceiveAsync(e);
				if (!pending)
					ProcessRecv(e);
            }
            else
            {
                try
                {
					token.Close();
                }
                catch (Exception ex)
                {
					Console.WriteLine("Already closed this socket.");
				}
            }
        }

		private void OnSessionClosed(CClientToken token)
        {
			// list에서 제거
			this.clientManager.Remove(token);

			if (this.RecvEventArgsPool != null)
				this.RecvEventArgsPool.Push(token.RecvEventArgs);

			if (this.SendEventArgsPool != null)
				this.SendEventArgsPool.Push(token.SendEventArgs);

			// recv, send 이벤트 null로 초기화
			token.SetEventArgs(null, null);

            Console.WriteLine("[Disconnect] " + token.ip + ":" + token.port);
        }
	}
}

