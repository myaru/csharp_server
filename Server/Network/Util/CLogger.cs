﻿using System;
using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace Network
{
    public static class CLogger
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(CLogger));

        public static void Init()
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            PatternLayout patternLayout = new PatternLayout();
            //patternLayout.ConversionPattern = "%date [%thread] %-5level %logger - %message%newline";
            patternLayout.ConversionPattern = "%date %-5level : %message%newline";
            patternLayout.ActivateOptions();

            RollingFileAppender roller = new RollingFileAppender();
            roller.AppendToFile = true;
            roller.File = "./log/log.log";
            roller.Layout = patternLayout;
            roller.RollingStyle = RollingFileAppender.RollingMode.Date;

            roller.DatePattern = "_yyyyMMdd";
            roller.PreserveLogFileNameExtension = true;
            roller.StaticLogFileName = false;

            roller.ActivateOptions();
            hierarchy.Root.AddAppender(roller);

            MemoryAppender memory = new MemoryAppender();
            memory.ActivateOptions();
            hierarchy.Root.AddAppender(memory);
            hierarchy.Configured = true;
        }

        public static void Debug(string log)
        {
            _log.Debug(log);
        }

        public static void Debug(string log, Exception exception)
        {
            _log.Debug(log, exception);
        }

        public static void Info(string log)
        {
            _log.Info(log);
        }

        public static void Info(string log, Exception exception)
        {
            _log.Info(log, exception);
        }

        public static void Error(string log)
        {
            _log.Error(log);
        }

        public static void Error(string log, Exception exception)
        {
            _log.Error(log, exception);
        }
    }
}

