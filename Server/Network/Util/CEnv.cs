﻿using System;
using dotenv.net;
using dotenv.net.Utilities;

/*
 # 인코딩 걸고 싶을 때
    using System.Text;
    ...
    DotEnv.Load(options: new DotEnvOptions(encoding: Encoding.ASCII));

 # .env 파일에서 공백 없이 불러오고 싶을 때 (디폴트 false)
    DotEnv.Load(options: new DotEnvOptions(trimValues: true));

 # .env 덮어쓰기(다시 불러올 때 사용하는 듯??)
    DotEnv.Load(options: new DotEnvOptions(overwriteExistingVars: false));


 # 불러오는 방법 1
    var envVars = DotEnv.Read();
    Console.WriteLine(envVars["KEY"]);

 # 불러오는 방법 2
    // 로드할 때 여러 옵션 넣어서 불러오기
    DotEnv.Fluent()
        .WithExceptions()
        .WithEnvFiles("./path/to/env")
        .WithTrimValues()
        .WithEncoding()
        .WithOverwriteExistingVars()
        .WithProbeForEnv(probeLevelsToSearch: 6)
        .Load();

    // 여기도 동일
    var envVars = DotEnv.Fluent()
        .WithoutExceptions()
        .WithEnvFiles() // revert to the default .env file
        .WithoutTrimValues()
        .WithDefaultEncoding()
        .WithoutOverwriteExistingVars()
        .WithoutProbeForEnv()
        .Read();

  # 자세한 내용은
    https://github.com/bolorundurowb/dotenv.net
    요기서 확인 가능
 */

namespace Network
{
    public static class CEnv
    {
        /// <summary>
        /// nuget위치로부터 몇 depth위까지 탐색할건지 (디폴트 4)
        /// </summary>
        /// <param name="depth"></param>
        public static void Init(int depth = 4)
        {
            DotEnv.Load(options: new DotEnvOptions(probeForEnv: true));
        }

        /// <summary>
        /// 여러개의 env를 받을 때 사용(1개여도 배열로 보내면 됨)
        /// </summary>
        /// <param name="path">.env 위치</param>
        public static void Init(string[] path)
        {
            DotEnv.Load(options: new DotEnvOptions(envFilePaths: path));
        }

        public static int GetInt(string key)
        {
            return EnvReader.GetIntValue(key);
        }

        public static double GetDouble(string key)
        {
            return EnvReader.GetDoubleValue(key);
        }

        public static bool GetBool(string key)
        {
            return EnvReader.GetBooleanValue(key);
        }

        public static string GetString(string key)
        {
            return EnvReader.GetStringValue(key);
        }
    }
}

