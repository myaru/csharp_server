﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Network
{
	public class CLogicThread : IMessageDispatcher
	{
		private CServer service;
		private ILogicQueue msgQueue;
		private AutoResetEvent logicEvent;

		public CLogicThread(CServer service)
		{
			this.service = service;
			this.msgQueue = new CDoubleBufferingQueue();
			this.logicEvent = new AutoResetEvent(false);
		}

		public void Strat()
        {
			Thread logic = new Thread(this.DoWork);
			logic.Start();
        }

        void IMessageDispatcher.OnMessage(CClientToken session, ArraySegment<byte> buffer)
        {
			// IO 스레드에서 호출 -> 완성된 패킷을 메시지 큐에 넣어줌
			CPacket packet = new CPacket(buffer, session);
			this.msgQueue.Enqueue(packet);

			// 로직스레드 이벤트 셋
			this.logicEvent.Set();
		}

		/// <summary>
        /// 워킹 스레드
        /// </summary>
        private void DoWork()
        {
            while (true)
            {
				// 패킷이 들어오면 이벤트 발생
				this.logicEvent.WaitOne();

				// 메시지 디스패치
				DispatchAll(this.msgQueue.GetAll());
            }
        }

		private void DispatchAll(Queue<CPacket> queue)
        {
            while (queue.Count > 0)
            {
				CPacket packet = queue.Dequeue();
				if (!this.service.clientManager.isExits(packet.clientSession))
					continue;
				
				packet.clientSession.OnMessage(packet);
            }
        }
    }
}

