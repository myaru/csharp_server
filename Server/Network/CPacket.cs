﻿using System;
using System.IO;

using ProtoBuf;

namespace Network
{
	public class CPacket
	{
		public CClientToken clientSession { get; private set; }
		public byte[] buffer { get; private set; }
		public int offset { get; private set; }
		public int size { get; private set; }

		public EProtocolID protocolID { get; private set; }

		public static CPacket Create(EProtocolID protocolID)
		{
			CPacket packet = new CPacket();
			packet.SetProtocolID(protocolID);
			return packet;
		}

		public CPacket()
		{
			buffer = new byte[Defines.PACKET_SIZE];
		}

		public CPacket(byte[] buffer, CClientToken session)
		{
			this.buffer = buffer;
			this.offset = Defines.HEADER_SIZE;
			this.clientSession = session;
		}

		public CPacket(ArraySegment<byte> buffer, CClientToken session)
		{
			this.buffer = buffer.Array;
			this.offset = Defines.HEADER_SIZE;
			this.size = buffer.Count;

			this.protocolID = (EProtocolID)GetProtocolID();
			this.offset = Defines.HEADER_SIZE;

			clientSession = session;
		}		

		/// <summary>
        /// 프로토콜 종류 패킹
        /// </summary>
        /// <param name="protocolID"></param>
		private void SetProtocolID(EProtocolID protocolID)
		{
			this.protocolID = protocolID;

			this.offset = Defines.HEADER_SIZE;
			Pack(protocolID);
		}

		/// <summary>
        /// 프로토콜 가져오는 함수
        /// </summary>
        /// <returns></returns>
		public ushort GetProtocolID()
		{
			ushort protocolID = BitConverter.ToUInt16(buffer, offset);
			offset += sizeof(UInt16);
			return protocolID;
            
		}

		/// <summary>
        /// 패킷에 내용 다 집어넣고 보내기전 패킷 맨 앞에 패킷 총 크기 넣는 함수
        /// </summary>
        public void SetHeader()
        {
			byte[] header = BitConverter.GetBytes(this.offset);
			header.CopyTo(this.buffer, 0);
        }

		/// <summary>
		/// Protocol ID 넣을때 사용함
		/// </summary>
		/// <param name="data"></param>
		private void Pack(EProtocolID data)
		{
			// 헤더는 send할때 넣을거
			byte[] tmpBuf = BitConverter.GetBytes((ushort)data);
			tmpBuf.CopyTo(this.buffer, this.offset);
			this.offset += tmpBuf.Length;

            //// 프로토콜ID 넣기
            //tmpBuf = BitConverter.GetBytes((ushort)data);
            //tmpBuf.CopyTo(this.buffer, this.offset);
            //this.offset += tmpBuf.Length;
        }

		/// <summary>
		/// Protobuf MSG 넣을때 사용함
		/// </summary>
		/// <param name="data"></param>
		public void Pack<T>(T data)
        {
			using (MemoryStream serialize = new MemoryStream())
            {
				Serializer.Serialize<T>(serialize, data);
				byte[] finalData = serialize.ToArray();
				finalData.CopyTo(this.buffer, this.offset);
				this.offset += finalData.Length;
			}			
		}

		public T UnPack<T>()
        {
			//byte[] tmpBuf = new byte[Defines.PACKET_SIZE - (Defines.HEADER_SIZE + sizeof(UInt16))];
			byte[] tmpBuf = new byte[this.buffer.Length - (this.offset + sizeof(ushort))];

			try
			{
				Buffer.BlockCopy(this.buffer, this.offset + sizeof(ushort), tmpBuf, 0, tmpBuf.Length);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				CLogger.Error(ex.Message);
			}

			using(MemoryStream deserialize = new MemoryStream(tmpBuf))
            {
				T data = Serializer.Deserialize<T>(deserialize);

				return data;
			}			
        }
	}
}

