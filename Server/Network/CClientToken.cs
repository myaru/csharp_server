﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Net.Sockets;

namespace Network
{
	public class CClientToken
	{
		private enum State
		{
			IDLE,               // 대기중
			CONNECT,            // 연결됨
			RESERVE_CLOSING,    // 종료가 예약됨 (send 리스트에 대기중인 상태에서 disconnect를 호출한 경우)
			CLOSE,              // 소켓 통신 종료됨
		}

		// close 중복처리 방지
		private int isClosed;

		// 클라이언트 상태
		private State curState;

		// 클라이언트 소켓
		public Socket socket { get; set; }

		// IO가 일어날 때 처리할 콜백
		public SocketAsyncEventArgs RecvEventArgs { get; private set; }
		public SocketAsyncEventArgs SendEventArgs { get; private set; }

		// 들어온 패킷을 다루기 쉽게 만들어줌
		private CMessageResolver msgResolver;

		// 서버, 클라에서 동일한 기능을 
		private ICallback callback;

		// BufferList를 위해 List로 구현
		private List<ArraySegment<byte>> sendList;
		private object sendingLock;

		// 싱글스레드로 처리할때 사용함
		private IMessageDispatcher dispatcher;

		public delegate void ClosedDelegate(CClientToken token);
		public ClosedDelegate OnSessionClosed;

		// HeartBeat
		public long lastHeartBeatTime { get; private set; }
		private CHeartBeat heartBeatSender;
		private bool autoHeartBeat;

		public string ip;
		public string port;

        public CClientToken(IMessageDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            this.sendingLock = new object();

            this.msgResolver = new CMessageResolver();
            this.callback = null;
            this.sendList = new List<ArraySegment<byte>>();
            this.lastHeartBeatTime = DateTime.Now.Ticks;

            this.curState = State.IDLE;
        }

		public void SetCallback(ICallback callback)
		{
			this.callback = callback;
		}

		public void SetEventArgs(SocketAsyncEventArgs recv, SocketAsyncEventArgs send)
		{
			this.RecvEventArgs = recv;
			this.SendEventArgs = send;
		}

		public void OnConnected()
		{
			this.curState = State.CONNECT;
			this.isClosed = 0;
			this.autoHeartBeat = true;
		}

		/// <summary>
		/// 패킷 분석은 별도 클래스로 빼서 처리시킴
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="transfered"></param>
		public void OnRecv(byte[] buffer, int offset, int transfered)
		{
			this.msgResolver.OnRecv(buffer, offset, transfered, OnMessageCompleted);
		}

		/// <summary>
		/// OnRecv에서 패킷을 읽어들이면 호출되는 함수
		/// </summary>
		/// <param name="buffer"></param>
		private void OnMessageCompleted(ArraySegment<byte> buffer)
		{
			if (this.callback == null)
				return;
			if (this.dispatcher != null)
				this.dispatcher.OnMessage(this, buffer);
			else
			{
				// OS의 IO스레드에서 호출
				CPacket packet = new CPacket(buffer, this);
				OnMessage(packet);
			}
		}

		/// <summary>
		/// 연결해제, 하트비트는 여기서 처리됨
		/// </summary>
		/// <param name="packet"></param>
		public void OnMessage(CPacket packet)
		{
			switch (packet.protocolID)
			{
				case EProtocolID.SYS_CLOSE_REQ:
					Disconnect();
					return;

                case EProtocolID.SYS_START_HEARTBEAT:
                    {
                        Console.WriteLine("[Recv] HeartBeat " + ip + ":" + port);

                        HeartBeat heartBeat = packet.UnPack<HeartBeat>();
                        this.heartBeatSender = new CHeartBeat(this, heartBeat.interval);

                        if (this.autoHeartBeat)
                            StartHeartbeat();
                    }                    
                    return;

                case EProtocolID.SYS_UPDATE_HEARTBEAT:
                    Console.WriteLine("[Recv] Update HeartBeat " + ip + ":" + port);
                    this.lastHeartBeatTime = DateTime.Now.Ticks;
                    return;
            }

			if (this.callback != null)
			{
				try
				{
					switch (packet.protocolID)
					{
						case EProtocolID.SYS_CLOSE_ACK:
							this.callback.OnRemoved();
							break;

						default:
							this.callback.OnMessage(packet);
							break;
					}
				}
				catch (Exception ex)
				{
					Close();
				}
			}

			if (packet.protocolID == EProtocolID.SYS_CLOSE_ACK)
			{
				if (this.OnSessionClosed != null)
					this.OnSessionClosed(this);
			}
		}

		public void Send(ArraySegment<byte> data)
		{
			lock (this.sendingLock)
			{
				this.sendList.Add(data);

				if (this.sendList.Count > 1)
					return;
			}

			StartSend();
		}

		public void Send(CPacket packet)
		{
			packet.SetHeader();
			Send(new ArraySegment<byte>(packet.buffer, 0, packet.offset));
		}

		private void StartSend()
		{
			try
			{
				this.SendEventArgs.BufferList = this.sendList;

                bool pending = this.socket.SendAsync(this.SendEventArgs);
				if (!pending)
					ProcessSend(this.SendEventArgs);				
			}
			catch (Exception e)
			{
				if (this.socket == null)
				{
					Close();
					return;
				}

				Console.WriteLine("Send Error : " + e.Message);
				throw new Exception(e.Message, e);
			}
		}

		/// <summary>
		/// 비동기 전송 완료시 호출되는 콜백 함수
		/// </summary>
		/// <param name="e"></param>
		public void ProcessSend(SocketAsyncEventArgs e)
		{
			if (e.BytesTransferred <= 0 || e.SocketError != SocketError.Success)
            {
				Console.WriteLine("Faile to Send");
                return; // 연결이 끊겨서 이미 종료된 소켓일것
            }
				

			lock (this.sendingLock)
			{
				// 리스트에 들어있는 데이터의 총 바이트 수
				var size = this.sendList.Sum(obj => obj.Count);

				// 전송이 완료되기전 추가전송을 요청하면 sendList에 더 있을것
				if (e.BytesTransferred != size)
				{
					// todo 세그먼트 하나를 다 못보낸 경우에 대한 처리
					if (e.BytesTransferred < this.sendList[0].Count)
					{
						Close();
						return;
					}

					// 보낼만큼 보내고 나머지 데이터는 한번에 보냄
					int sent_index = 0;
					int sum = 0;
					for (int i = 0; i < this.sendList.Count; i++)
					{
						sum += this.sendList[i].Count;
						if (sum <= e.BytesTransferred)
						{
							// 전송완료된 데이터 인덱스
							sent_index = i;
							continue;
						}
						break;
					}

					// 전송 완료했으면 리스트에서 삭제
					this.sendList.RemoveRange(0, sent_index + 1);

					// 나머지 데이터들 보내기
					StartSend();
					return;
				}

				// 다 보냈으니 초기화
				this.sendList.Clear();

				// 종료가 예약되있으면 소켓 릴리즈
				if (this.curState == State.RESERVE_CLOSING)
					this.socket.Shutdown(SocketShutdown.Send);
			}
		}


		public void Close()
		{
			// 중복으로 호출되는거 방지
			if (Interlocked.CompareExchange(ref this.isClosed, 1, 0) == 1)
				return;

			// 이미 종료됨
			if (this.curState == State.CLOSE)
				return;

			this.curState = State.CLOSE;
			this.socket.Close();
			this.socket = null;

			this.SendEventArgs.UserToken = null;
			this.RecvEventArgs.UserToken = null;

			this.sendList.Clear();
			this.msgResolver.ClearBuffer();

			if (this.callback != null)
			{
				CPacket packet = CPacket.Create(EProtocolID.SYS_CLOSE_ACK);
				if (this.dispatcher != null)
					this.dispatcher.OnMessage(this, new ArraySegment<byte>(packet.buffer, 0, packet.offset));
				else
					OnMessage(packet);
			}
		}

		public void Disconnect()
		{
			try
			{
				if (this.sendList.Count <= 0)
				{
					this.socket.Shutdown(SocketShutdown.Send);
					return;
				}

				this.curState = State.RESERVE_CLOSING;
			}
			catch (Exception ex)
			{
				Close();
			}
		}

		public void Kick()
		{
			try
			{
				CPacket packet = CPacket.Create(EProtocolID.SYS_CLOSE_REQ);
				Send(packet);
			}
			catch (Exception ex)
			{
				Close();
			}
		}

		public bool isConnectd()
		{
			return this.curState == State.CONNECT;
		}

		public void StartHeartbeat()
		{
			if (this.heartBeatSender != null)
				this.heartBeatSender.Start();
		}

		public void StopHeartbeat()
		{
			if (this.heartBeatSender != null)
				this.heartBeatSender.Stop();
		}

		public void DisableAutoHeartbeat()
		{
			StopHeartbeat();
			this.autoHeartBeat = false;
		}

		public void UpdateHeartBeatManually(float time)
		{
			if (this.heartBeatSender != null)
				this.heartBeatSender.Update(time);
		}
	}
}
