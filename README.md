# ppyamttaeligi Server

## 설치 방법
```
cd project_dir
git clone https://gitlab.com/slavedev/SlaveDevServer.git
```


## 개발 환경
- C# .net 6.0
- MySQL
- Redis

## NuGet 패키지
- dotent.net
- log4net
- protobuf-net
- StackExchange.Redis
- StackExchange.REdis.Extensions.Newtonsoft
- FreeSql
- FreeSql.DbContext
- FreeSql.Provider.MySqlConnector

# 기타 설명
## 패킷 생성/언팩 방법
- 패킷 생성
```
CPacket packet = CPacket.Create(EProtocolID.[PACKET_CLASS])
```
- 패킷 언팩
```
PACKET_CLASS packet = packet.UnPack<PACKET_CLASS>
```

## 로거
- 프로그램 최초 시작시 초기화 필수
```
CLogger.Error(string)
CLogger.Error(string, Exception)

CLogger.Info(string)
CLogger.Info(string, Exception)

CLogger.Debug(string)
CLogger.Debug(string, Exception)
```
- ./log/log.log 에 기록 됨
